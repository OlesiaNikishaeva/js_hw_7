function filterBy(arr, dataType) {

  const resultArr = [];

  arr.forEach((elem) => {
    if (typeof (elem) !== dataType) {
      resultArr.push(elem)
    }
  })

  return resultArr;
}

console.log(filterBy(['hello', 'world', 23, '23', null], 'string'));